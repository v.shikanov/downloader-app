<?php

namespace Tests\Feature\Jobs;

use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Models\Job as JobModel;
use App\Models\File as FileModel;
use App\Jobs\Downloader as DownloaderJob;
use App\Services\Downloader as DownloaderService;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\Psr7\Response;
use Storage;

class DownloaderTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @expectedException \App\Exceptions\Resource\DownloadException
     *
     * @return void
     */
    public function testThrowsClientExceptionOnNotFound(): void
    {
        $mock = new MockHandler([
            new Response(404, [
                'Content-Type' => 'text/plain',
                'Content-Length' => 3,
            ], 'foo'),
        ]);
        $handler = HandlerStack::create($mock);
        $httpClient = new \App\Services\Http\Client(['handler' => $handler]);

        $this->app->instance(DownloaderService::class, new DownloaderService($httpClient));

        $jobModel = factory(JobModel::class)->state('new')->create([
            'url' => 'https://valid.com/foo.txt',
        ]);

        DownloaderJob::dispatch($jobModel);
    }

    /**
     * @return void
     */
    public function testDownloadsAndStoresFileOnOk(): void
    {
        Storage::fake();

        $mock = new MockHandler([
            new Response(200, [
                'Content-Type' => 'text/plain',
                'Content-Length' => 3,
            ], 'bar'),
        ]);
        $handler = HandlerStack::create($mock);
        $httpClient = new \App\Services\Http\Client(['handler' => $handler]);

        $this->app->instance(DownloaderService::class, new DownloaderService($httpClient));

        /**
         * @var JobModel $jobModel
         */
        $jobModel = factory(JobModel::class)->state('new')->create([
            'url' => 'https://valid.com/foo.txt',
        ]);

        DownloaderJob::dispatch($jobModel);
        $jobModel->refresh();
        $this->assertInstanceOf(FileModel::class, $jobModel->file);
        $this->assertSame('text/plain', $jobModel->file->mime);
        $this->assertSame(3, $jobModel->file->size);
        Storage::assertExists($jobModel->file->filename);
        $this->assertSame('bar', Storage::get($jobModel->file->filename));
    }
}
