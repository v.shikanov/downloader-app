<?php

use Faker\Generator as Faker;
use App\Models\Job as JobModel;

$factory->define(JobModel::class, function (Faker $faker) {
    return [
        'url' => $faker->url,
        'started_at' => $faker->dateTime,
        'finished_at' => $faker->dateTime,
        'error' => $faker->sentence,
    ];
});

$factory->state(JobModel::class, 'new', function (Faker $faker) {
    return [
        'status' => JobModel::STATUS_NEW,
    ];
});

$factory->state(JobModel::class, 'started', function (Faker $faker) {
    return [
        'status' => JobModel::STATUS_STARTED,
    ];
});

$factory->state(JobModel::class, 'finished', function (Faker $faker) {
    return [
        'status' => JobModel::STATUS_FINISHED,
    ];
});

$factory->state(JobModel::class, 'failed', function (Faker $faker) {
    return [
        'status' => JobModel::STATUS_FAILED,
    ];
});
