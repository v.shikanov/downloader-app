<?php

use Faker\Generator as Faker;
use App\Models\File as FileModel;
use App\Models\Job as JobModel;

$factory->define(FileModel::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'filename' => $faker->image('/tmp', 640, 480, null, false),
        'size' => $faker->randomNumber(),
        'mime' => $faker->mimeType,
        'job_id' => function () {
            return factory(JobModel::class)->create()->id;
        }
    ];
});
