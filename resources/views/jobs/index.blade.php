@extends('layouts.layout')

@section('title', 'Tasks List')

@section('content')
    <h1>Jobs List</h1>

    @if (count($jobs))
    <table class="table table-striped">
        <thead>
        <tr>
            <th scope="col">Url</th>
            <th scope="col">Link</th>
            <th scope="col">Status</th>
            <th scope="col">Started At</th>
            <th scope="col">Finished At</th>
            <th scope="col">Created At</th>
        </tr>
        </thead>
        <tbody>

        @foreach($jobs as $job)
        <tr>
            <td>{{ $job->url }}</td>
            <td class="text-justify">
                @if ($job->isFinished())
                    <a href="{{ app('services.resource')->getDownloadLink($job->file) }}">Download</a>
                @endif
            </td>
            <td>{{ $job->human_status }}</td>
            <td>{{ $job->started_at }}</td>
            <td>{{ $job->finished_at }}</td>
            <td>{{ $job->created_at }}</td>
        </tr>

        @endforeach

        </tbody>
    </table>
    @else
        <p>Job queue is empty.</p>
    @endif

@endsection
