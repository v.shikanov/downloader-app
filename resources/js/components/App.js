import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import List from "./Jobs/List";
import Create from "./Jobs/Create";

export default class App extends Component {
    render() {
        return (
            <Router>

            <div>
                <nav className="navbar navbar-expand-lg navbar-light bg-light" style={{paddngBottom: 30}}>
                    <a className="navbar-brand" href="#">Downloader App</a>
                    <button className="navbar-toggler" type="button" data-toggle="collapse"
                            data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                            aria-expanded="false" aria-label="Toggle navigation">
                        <span className="navbar-toggler-icon"> </span>
                    </button>

                    <div className="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul className="navbar-nav mr-auto">
                            <li className="nav-item active">
                                <Link className="nav-link" to="/jobs/list">Jobs List</Link>
                            </li>
                            <li className="nav-item">
                                <Link className="nav-link" to="/jobs/create">Enqueue New Job</Link>
                            </li>
                        </ul>
                    </div>
                </nav>

                <div className="container">
                    <Route path="/jobs/list" component={List} />
                    <Route path="/jobs/create" component={Create} />
                </div>
            </div>
            </Router>
        );
    }
}

if (document.getElementById('app')) {
    ReactDOM.render(<App />, document.getElementById('app'));
}
