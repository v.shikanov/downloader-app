import React, {Component} from 'react';
import axios from 'axios';

export default class List extends Component {
    constructor(props) {
        super(props);
        this.state = {
            url: '',
            errors: false,
        };

        this.handleUrl = this.handleUrl.bind(this);
        this.clickEnqueueButton = this.clickEnqueueButton.bind(this);
    }

    render() {
        return (
            <div>
                <h4>Enqueue New Job</h4>

                {this.state.jobs && this.state.jobs.result === 'ok' ? this.state.jobs.message :
                <form>
                    <div className="form-group">
                        <label htmlFor="url">URL</label>
                        <input type="text" className="form-control" id="url" name="url" placeholder="URL"
                               required
                               onChange={this.handleUrl}
                        />
                    </div>
                    <button type="submit" className="btn btn-primary" onClick={this.clickEnqueueButton}>Enqueue</button>
                </form>
                }

            </div>
        );
    }

    handleUrl(event) {
        this.setState({url: event.target.value})
    }


    clickEnqueueButton(e) {
        e.preventDefault();
        axios.post('/api/v1/jobs', {url: this.state.url})
            .then((response) => {
                console.log(response.data);
                this.setState({jobs: response.data})
            })
            .catch(error => {
                this.setState({errors: error.response.url})
            });
    }
}
