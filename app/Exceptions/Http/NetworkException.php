<?php
declare(strict_types=1);

namespace App\Exceptions\Http;

use Psr\Http\Client\NetworkExceptionInterface;
use Psr\Http\Message\RequestInterface;

class NetworkException extends \GuzzleHttp\Exception\RequestException implements NetworkExceptionInterface
{
    /**
     * @return \Psr\Http\Message\RequestInterface
     */
    public function getRequest(): RequestInterface
    {
        return parent::getRequest();
    }
}
