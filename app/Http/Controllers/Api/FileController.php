<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\Api\File\Show as ShowFileRequest;
use App\Models\File as FileModel;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\File as FileResource;

class FileController extends Controller
{
    /**
     * @param \Illuminate\Http\Request $request
     * @return AnonymousResourceCollection
     */
    public function index(Request $request): AnonymousResourceCollection
    {
        return FileResource::collection(FileModel::all());
    }

    /**
     * @param ShowFileRequest $request
     * @param int $id
     * @return FileResource
     */
    public function show(ShowFileRequest $request, int $id): FileResource
    {
        return FileResource::make(FileModel::find($id));
    }

    /**
     * @param ShowFileRequest $request
     * @param int $id
     * @return BinaryFileResponse
     */
    public function download(ShowFileRequest $request, int $id): BinaryFileResponse
    {
        return FileResource::make(FileModel::find($id))->download();
    }
}
