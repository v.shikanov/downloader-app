<?php

namespace App\Http\Controllers;

use App\Http\Requests\Job\Store as StoreJobRequest;
use App\Models\Job as JobModel;
use App\Services\Resource as ResourceService;
use Illuminate\Http\Request;
use Psr\Container\ContainerInterface;

class JobController extends Controller
{
    /**
     * @var ResourceService
     */
    private $resourceService;

    /**
     * @param ContainerInterface $container
     * @return void
     */
    public function __construct(ContainerInterface $container)
    {
        $this->resourceService = $container->get('services.resource');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        return view('jobs.index')
            ->with('jobs', JobModel::with('file')->get());
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create(Request $request)
    {
        return view('jobs.create');
    }

    /**
     * @param \App\Http\Requests\Job\Store $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(StoreJobRequest $request)
    {
        $this->resourceService->createDownloadJob($request->get('url'));

        session()->flash('enqueued', 'Job to download resource has been enqueued.');

        return view('jobs.create');
    }
}
