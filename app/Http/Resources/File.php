<?php

namespace App\Http\Resources;

use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Storage;

class File extends AbstractResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request): array
    {
        return array_merge(parent::toArray($request), [
            'download_link' => resolve('services.resource')->getDownloadLink($this->resource),
        ]);
    }

    /**
     * @return BinaryFileResponse
     */
    public function download(): BinaryFileResponse
    {
        return response()->download(
            Storage::path($this->resource->filename),
            $this->resource->name,
            [
                'Content-Type' => $this->resource->mime
            ]
        );
    }
}
