<?php

namespace App\Jobs;

use App\Exceptions\Downloader\UnableToStoreException;
use App\Models\Job as JobModel;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Storage;
use Exception;
use GuzzleHttp\Psr7;
use App\Services\Downloader as DownloaderService;

class Downloader implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var JobModel
     */
    protected $jobModel;

    /**
     * Create a new job instance.
     *
     * @param JobModel $jobModel
     * @return void
     */
    public function __construct(JobModel $jobModel)
    {
        $this->jobModel = $jobModel;
    }

    /**
     * Execute the job.
     *
     * @param DownloaderService $downloaderService
     * @return void
     * @throws UnableToStoreException|\App\Exceptions\Resource\DownloadException
     */
    public function handle(DownloaderService $downloaderService): void
    {
        $this->jobModel->markStarted();

        $uri = new Psr7\Uri($this->jobModel->url);
        $pathParts = pathinfo($uri->getPath());
        $filename = md5($pathParts['basename']);

        $downloaderService->setUri($uri);
        $resource = $downloaderService->getResource();

        if (! Storage::put($filename, $resource->getBody()->getContents(), 'public')) {
            throw new UnableToStoreException('Unable to store file into the storage.');
        }

        $this->jobModel->markFinished();
        $this->jobModel->file()->create([
            'name' => $pathParts['basename'],
            'filename' => $filename,
            'mime' => Storage::mimeType($filename),
            'size' => Storage::size($filename),
        ]);
    }

    /**
     * @param \Exception $exception
     * @return void
     */
    public function failed(Exception $exception): void
    {
        $this->jobModel->markFailed($exception->getMessage());
    }
}
