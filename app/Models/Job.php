<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * App\Models\Job
 *
 * @property-read \App\Models\File $file
 * @property-read string $human_status
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Job newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Job newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Job query()
 * @mixin \Eloquent
 * @property int $id
 * @property string $url
 * @property int $status
 * @property string|null $started_at
 * @property string|null $finished_at
 * @property string|null $error
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Job whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Job whereError($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Job whereFinishedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Job whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Job whereStartedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Job whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Job whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Job whereUrl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Job ofStatus($status)
 */
class Job extends Model
{
    public const STATUS_NEW = 0;
    public const STATUS_STARTED = 1;
    public const STATUS_FINISHED = 2;
    public const STATUS_FAILED = 3;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'url',
        'status',
        'started_at',
        'finished_at',
        'error',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [

    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'started_at' => 'datetime',
        'finished_at' => 'datetime',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function file(): HasOne
    {
        return $this->hasOne(File::class);
    }

    /**
     * @return void
     */
    public function markStarted(): void
    {
        $this->update([
            'started_at' => now(),
            'status' => self::STATUS_STARTED,
        ]);
    }

    /**
     * @return void
     */
    public function markFinished(): void
    {
        $this->update([
            'finished_at' => now(),
            'status' => self::STATUS_FINISHED,
        ]);
    }

    /**
     * @param $message
     * @return void
     */
    public function markFailed($message): void
    {
        $this->update([
            'status' => self::STATUS_FAILED,
            'error' => $message,
        ]);
    }

    /**
     * @return bool
     */
    public function isNew(): bool
    {
        return $this->status === self::STATUS_NEW;
    }

    /**
     * @return bool
     */
    public function isStarted(): bool
    {
        return $this->status === self::STATUS_STARTED;
    }

    /**
     * @return bool
     */
    public function isFinished(): bool
    {
        return $this->status === self::STATUS_FINISHED;
    }

    /**
     * @return bool
     */
    public function isFailed(): bool
    {
        return $this->status === self::STATUS_FAILED;
    }

    /**
     * Scope a query to only include jobs of a given status.
     *
     * @param  \Illuminate\Database\Eloquent\Builder $query
     * @param  mixed $status
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeOfStatus($query, $status): Builder
    {
        return $query->where('status', $status);
    }

    /**
     * @return string
     */
    public function getHumanStatusAttribute(): string
    {
        switch ($this->status) {
            case self::STATUS_NEW:
                return 'New';
            case self::STATUS_STARTED:
                return 'Started';
            case self::STATUS_FINISHED:
                return 'Finished';
            case self::STATUS_FAILED:
                return 'Error';
            default:
                return 'Unknown';
        }
    }
}
