<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group([
    'prefix' => 'v1',
    'namespace' => 'Api',
], function () {
    Route::name('api.')->group(function () {
        Route::apiResource('files', 'FileController')->only([
            'index',
            'show',
        ])->parameter('files', 'id');

        Route::get('files/{id}/download', 'FileController@download')->name('files.download');

        Route::apiResource('jobs', 'JobController')->only([
            'index',
            'store',
            'show',
        ])->parameter('jobs', 'id');
    });
});
